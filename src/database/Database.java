/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author informatics
 */
public class Database {

    static String url = "jdbc:sqlite:./db/library.db";
    static Connection condb = null;

    public static Connection connectdb() {
        if (condb != null) {
            return condb;
        }
        try {
            condb = DriverManager.getConnection(url);
            System.out.println("Connected");
            return condb;
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void closedb() {
        try {
            if (condb != null) {
                condb.close();
                System.out.println("Closed");
                condb = null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
