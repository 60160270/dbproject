/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbproject;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author informatics
 */
public class Dbproject {

    static String url = "jdbc:sqlite:./db/library.db";
    static Connection condb = null;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        condb = Database.connectdb();
        try {
            Statement stm = condb.createStatement();
            String sql = "SELECT userId,\n"
                    + "       loginName,\n"
                    + "       password,\n"
                    + "       name,\n"
                    + "       surname,\n"
                    + "       typeId\n"
                    + "  FROM User";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getInt("userId") + " " + rs.getString("loginName"));
            }
        } catch (SQLException ex) {
            //Logger.getLogger(Dbproject.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.closedb();

    }

    private static Connection connectdb() {
        if (condb != null) {
            return condb;
        }
        try {
            condb = DriverManager.getConnection(url);
            System.out.println("Connected");
            return condb;
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private static void closedb() {
        try {
            if (condb != null) {
                condb.close();
                System.out.println("Closed");
                condb = null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
